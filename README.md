# IPL Data Project

## Aim

In this data assignment we will transform raw data of IPL to calculate the following stats:

- Number of matches played per year for all the years in IPL.
- Number of matches won per team per year in IPL.
- Extra runs conceded per team in the year 2016
- Top 10 economical bowlers in the year 2015
  Create 4 functions and the results of the functions will dump JSON files in the output folder.

# Prerequisite

This project uses the 'csvtojson' library, to install this, run the command:
`npm install`

# Run the project

To run the project run the command:
`npm run start`

This will create/update JSON data files in the output folder.
