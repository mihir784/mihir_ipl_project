export function matchesPlayedPerYear(matches) {
  const matchesPlayed = {};

  for (let match of matches) {
    const season = match.season;

    if (matchesPlayed[season]) {
      matchesPlayed[season] += 1;
    } else {
      matchesPlayed[season] = 1;
    }
  }

  return matchesPlayed;
}

export function matchesWonByTeamPerYear(matches) {
  const matchesWon = {};

  for (let match of matches) {
    const winner = match.winner ? match.winner : "No Result";

    if (!matchesWon[match.season]) {
      matchesWon[match.season] = {};
    }

    if (matchesWon[match.season][winner]) {
      matchesWon[match.season][winner] += 1;
    } else {
      matchesWon[match.season][winner] = 1;
    }
  }

  return matchesWon;
}

function getMatchYear(matches, match_id) {
  for (let match of matches) {
    if (match.id == match_id) {
      return match.season;
    }
  }
}

export function extrasIn2016(matches, deliveries) {
  const extras = {};

  for (let delivery of deliveries) {
    const team = delivery.bowling_team;
    const extraRuns = parseInt(delivery.extra_runs);

    if (extraRuns > 0) {
      let match_year = getMatchYear(matches, delivery.match_id);

      if (match_year == 2016) {
        extras[team] = (extras[team] || 0) + extraRuns;
      }
    }
  }
  return extras;
}

export function economicBowlersIn2015(matches, deliveries) {
  const result = {};

  for (let delivery of deliveries) {
    const bowler = delivery.bowler;

    if (!result[bowler]) {
      result[bowler] = { name: bowler };
    }

    const runs = parseInt(delivery.total_runs);
    let match_year = getMatchYear(matches, delivery.match_id);

    if (match_year == 2015) {
      result[bowler]["runsGiven"] = (result[bowler]["runsGiven"] || 0) + runs;
      result[bowler]["ballsBowled"] = (result[bowler]["ballsBowled"] || 0) + 1;
    }
  }

  let arrayResult = [];
  for (let bowler in result) {
    let overs = Math.floor(result[bowler]["ballsBowled"] / 6);

    result[bowler]["economy"] = result[bowler]["runsGiven"] / overs;

    arrayResult.push([
      result[bowler]["name"],
      result[bowler]["economy"] ? result[bowler]["economy"] : Infinity,
      result[bowler]["runsGiven"],
      overs,
    ]);

    delete result[bowler];
  }

  arrayResult.sort((a, b) => {
    return a[1] - b[1];
  });

  let economicBowlersIn2015 = {};

  for (let i = 0; i < 10; i++) {
    economicBowlersIn2015[arrayResult[i][0]] = arrayResult[i][1];
  }

  return economicBowlersIn2015;
}
