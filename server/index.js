import fs from "fs";
import csvtojson from "csvtojson";

import {
  matchesPlayedPerYear,
  matchesWonByTeamPerYear,
  extrasIn2016,
  economicBowlersIn2015,
} from "../server/ipl.js";

const MATCHES_FILE_PATH = "./data/matches.csv";
const DELIVERIES_FILE_PATH = "./data/deliveries.csv";

const OUTPUT_FILE_1_PATH = "./public/output/matchesPlayedPerYear.json";
const OUTPUT_FILE_2_PATH = "./public/output/matchesWonByTeamPerYear.json";
const OUTPUT_FILE_3_PATH = "./public/output/extrasIn2016.json";
const OUTPUT_FILE_4_PATH = "./public/output/economicBowlersIn2015.json";

function saveDataInFile(result, filePath) {
  fs.writeFile(filePath, JSON.stringify(result), "utf8", (err) => {
    if (err) console.log(err);
  });
}

function createJsonDataFiles() {
  csvtojson()
    .fromFile(MATCHES_FILE_PATH)
    .then((matches) => {
      saveDataInFile(matchesPlayedPerYear(matches), OUTPUT_FILE_1_PATH);

      saveDataInFile(matchesWonByTeamPerYear(matches), OUTPUT_FILE_2_PATH);

      csvtojson()
        .fromFile(DELIVERIES_FILE_PATH)
        .then((deliveries) => {
          saveDataInFile(extrasIn2016(matches, deliveries), OUTPUT_FILE_3_PATH);

          saveDataInFile(
            economicBowlersIn2015(matches, deliveries),
            OUTPUT_FILE_4_PATH
          );
        });
    });
}

createJsonDataFiles();
